<?php
/**
 * @file
 * Instagram Pane Ctools Definition.
 */

/**
 * Ctools plugin array.
 *
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Instagram'),
  'category' => t('Content'),
  'all contexts' => TRUE,
);

/**
 * Render the custom content type.
 */
function instagram_pane_instagram_content_type_render($subtype, $conf, $panel_args, $context) {

  $client_id = variable_get('instagram_pane_client_id', '');
  if (empty($client_id)) {
    drupal_set_message(t('Set your Instagram API config.'), 'warning');
    return FALSE;
  }

  $instagram_term = ctools_context_keyword_substitute($conf['search'], array(), $context);

  // Don't show the pane if there is no search term.
  // For example if the keyword replace returns nothing.
  if (empty($instagram_term)) {
    return FALSE;
  }

  // Get the feed.
  $feed_markup = _instagram_pane_get_feed($instagram_term, $conf['count'], $conf);

  // Don't show the pane if the feed returns false.
  if (!$feed_markup) {
    return FALSE;
  }

  // Generate the title.
  if ($conf['search_type'] == 'tag') {
    $instagram_link_destination = 'https://instagram.com/explore/tags/';
  }
  else {
    $instagram_link_destination = 'https://instagram.com/';
  }
  $instagram_link_options = array(
    'attributes' => array(
      'target' => '_blank',
    ),
  );
  $instagram_link = l($instagram_term, $instagram_link_destination . $instagram_term, $instagram_link_options);
  $title_replacements = array(
    '!link' => $instagram_link,
  );
  $title = t('Instagram: !link', $title_replacements);

  // Put it all together.
  $content_vars = array(
    'title' => $title,
    'feed' => $feed_markup,
  );
  $content = theme('instagram_panel', $content_vars);

  // Build the content type block.
  $block = new stdClass();
  $block->module  = 'instagram_pane';
  $block->delta   = 'instagram';
  $block->content = $content;
  return $block;
}


/**
 * Returns the administrative title for a type.
 */
function instagram_pane_instagram_content_type_admin_title($subtype, $conf, $context) {
  return t('Instagram');
}

/**
 * Helper function to call the Instagram API and process the results.
 */
function _instagram_pane_get_feed($search_term, $count, $conf) {

  $client_id = variable_get('instagram_pane_client_id');
  $search_id = '';
  $cache_id = drupal_clean_css_identifier('instagram pane ' . $conf['search_type'] . ' ' . $search_term . ' ' . $count);

  // Load the data from the cache if it's still current.
  if ($cached = cache_get($cache_id, 'cache')) {
    $current_time = time();
    if ($current_time < $cached->expire) {
      return $cached->data;
    }
  }

  // Have to find the user id number if we're searching for users.
  if ($conf['search_type'] == 'user') {
    $url_search = 'https://api.instagram.com/v1/users/search';

    // Get id for user.
    $query = array(
      'q' => $search_term,
      'client_id' => $client_id,
    );
    $request_url = url($url_search, array('query' => $query));
    $response_search = drupal_http_request($request_url);

    if ($response_search->code != '200') {
      watchdog('instagram_pane', $response_search->data, array(), WATCHDOG_ERROR);
      return FALSE;
    }

    // Get the ID from the response.
    $search_data = json_decode($response_search->data);

    // Sometimes we get a 200 but with no actual data.
    if (isset($search_data->data[0])) {
      $search_id = $search_data->data[0]->id;
    }
    else {
      $replacements = array('!term' => $search_term);
      $message = 'Query for "!term" returned 200 response but with no data';
      watchdog('instagram_pane', $message, $replacements, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  // Get feed.
  // Use the correct Feed type.
  if ($conf['search_type'] == 'tag') {
    $url_feed = "https://api.instagram.com/v1/tags/{$search_term}/media/recent";
  }
  else {
    $url_feed = "https://api.instagram.com/v1/users/{$search_id}/media/recent";
  }

  $query = array(
    'client_id' => $client_id,
    'count' => $count,
  );
  $request_url = url($url_feed, array('query' => $query));
  $response_feed = drupal_http_request($request_url);

  if ($response_feed->code != '200') {
    watchdog('instagram_pane', $response_feed->data, array(), WATCHDOG_ERROR);
    return FALSE;
  }

  $feed_data = json_decode($response_feed->data);

  if (count($feed_data->data) == 0) {
    return FALSE;
  }

  $markup = '';
  foreach ($feed_data->data as $item) {
    $item_markup = '';
    if ($item->type == 'video') {
      $item_markup = theme('instagram_video', array('item' => $item));
    }
    elseif ($item->type == 'image') {
      $item_markup = theme('instagram_image', array('item' => $item));
    }
    else {
      $item_markup = t('Unrecognised media type');
    }

    $markup .= $item_markup;
  }

  // Cache the markup.
  $cache_time = variable_get('instagram_pane_cache_time');
  $expiration_string = "+$cache_time seconds";
  cache_set($cache_id, $markup, 'cache', strtotime($expiration_string));

  return $markup;
}

/**
 * Create pane conf form.
 */
function instagram_pane_instagram_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $search_value = '';
  if (isset($conf['search'])) {
    $search_value = filter_xss_admin($conf['search']);
  }

  $form['search'] = array(
    '#type' => 'textfield',
    '#title' => t('Search'),
    '#default_value' => $search_value,
    '#description' => t('Enter the username or hashtag to display. No @ or # symbols needed. You can also use %keywords.'),
    '#required' => TRUE,
  );

  $radio_options = array(
    'user' => t('Username'),
    'tag' => t('Hashtag'),
  );
  $form['search_type'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#default_value' => isset($conf['search_type']) ? $conf['search_type'] : 0,
    '#options' => $radio_options,
  );

  $form['count'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of items'),
    '#default_value' => isset($conf['count']) ? $conf['count'] : 8,
    '#description' => t('The number of items you want to be displayed'),
  );

  // Remove default pane title form.
  unset($form['override_title']);
  unset($form['override_title_heading']);
  unset($form['override_title_text']);
  unset($form['override_title_markup']);

  return $form;
}

/**
 * Save conf form values.
 */
function instagram_pane_instagram_content_type_edit_form_submit($form, &$form_state) {
  foreach (element_children($form) as $key) {
    if (!empty($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
    else {
      if (isset($form_state['conf'][$key])) {
        unset($form_state['conf'][$key]);
      }
    }
  }
}

/**
 * Show the title of the block in the panels content page.
 */
function instagram_pane_instagram_content_type_admin_info($subtype, $conf, $context = NULL) {
  $block = new stdClass();
  $title_value = '';
  if (isset($conf['search'])) {
    $title_value = filter_xss_admin($conf['search']);
  }
  $block->title = $title_value;
  return $block;
}
