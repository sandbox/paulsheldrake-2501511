<?php
/**
 * @file
 * Admin page forms.
 */

/**
 * Implements hook_form().
 */
function instagram_pane_settings_form($form, &$form_state) {

  $link = l(t('go to this URL'), 'https://instagram.com/developer/clients/register/');
  $replacements = array(
    '!reg_link' => $link,
  );

  $form['intro'] = array(
    '#type' => 'item',
    '#title' => t('Setup'),
    '#markup' => t('To get a client ID/secret !reg_link and set one up. You will need to logged in to your twitter account.', $replacements),
  );
  $form['instagram_pane_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('instagram_pane_client_id', ''),
  );
  $form['instagram_pane_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret'),
    '#required' => TRUE,
    '#default_value' => variable_get('instagram_pane_client_secret', ''),
  );

  $cache_time_options = array(
    '0' => t('None'),
    '30' => t('30 sec'),
    '60' => t('1 min'),
    '120' => t('2 min'),
    '180' => t('3 min'),
    '240' => t('4 min'),
    '300' => t('5 min'),
    '600' => t('10 min'),
    '900' => t('15 min'),
    '1200' => t('20 min'),
    '1800' => t('30 min'),
    '3600' => t('1 hour'),
    '86400' => t('1 day'),
    '604800' => t('1 week'),
  );
  $form['instagram_pane_cache_time'] = array(
    '#type' => 'select',
    '#title' => t('Cache time'),
    '#options' => $cache_time_options,
    '#default_value' => variable_get('instagram_pane_cache_time'),
    '#description' => t("It's recommended to set a cache time for the responses to improve performance and not waste your API calls."),
  );
  return system_settings_form($form);
}
