<?php
/**
 * @file
 * Template file for panel.
 */
?>


<div class="panel panel-default pane-instagram">
  <div class="panel-heading"><?php print $title; ?></div>
  <div class="panel-body">
      <div class="row">
        <?php print $feed; ?>
      </div>
  </div>
</div>
