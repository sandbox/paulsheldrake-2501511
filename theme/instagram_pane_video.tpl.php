<?php
/**
 * @file
 * Template file for instagram video.
 */
?>

<div class="col-xs-4 col-sm-3">

  <a class="thumbnail" data-toggle="modal" data-target="#<?php print $modal_id; ?>" >
    <img src="<?php print $src_poster; ?>" class="img-responsive">
  </a>

  <div class="modal fade instagram-pane-modal" id="<?php print $modal_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          &nbsp;
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <video src="<?php print $src;?>" controls poster="<?php print $src_poster;?>" class="img-responsive">
            <?php print t("Sorry, your browser doesn't support embedded videos."); ?>
          </video>
          <?php print $caption; ?>
        </div>
      </div>
    </div>
  </div>

</div>
